# This should be a test startup script
require sendfile

epicsEnvSet("IOCNAME", "TEST")
epicsEnvSet("PORT", "L0")
epicsEnvSet("ADDRESS", "24")

iocshLoad("$(sendfile_DIR)/sendfile.iocsh")

iocInit
