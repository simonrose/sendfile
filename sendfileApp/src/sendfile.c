#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

#include <http.h>

static long subSendFileInit(aSubRecord *prec)
{
    printf("============\n"
           "Initialized!\n"
           "============\n");
    return 0;
}

static long subSendFileProc(aSubRecord *prec)
{
    int status;
    printf("==========\n"
           "Processed!\n"
           "==========\n");

    httpVar_t filename = {"filename", "file.txt"};
    status = httpSendFile(prec->a, *(epicsInt32 *)prec->b, prec->c, filename, prec->d);
    return status;
}

epicsRegisterFunction(subSendFileInit);
epicsRegisterFunction(subSendFileProc);