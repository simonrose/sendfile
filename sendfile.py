import argparse
import sys
from epics import PV

parser = argparse.ArgumentParser("Sends a file via EPICS HTTP post module")
parser.add_argument("filename", help="File to send")
parser.add_argument("iocname", help="IOC name")

args = parser.parse_args(sys.argv[1:])

trigger = PV(args.iocname + ":HTTP_POST.PROC")
cof_pv = PV(args.iocname + ":COF_FILE")

with open(args.filename, "r") as f:
    cof_pv.put(f.read())

trigger.put(1)
